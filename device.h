#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef DEVICE_H_
#define DEVICE_H_

struct Handle{
    char signature[4], name[32];
    int blocksize, blockcount;
};

struct Handle deviceHandle[10];

FILE *files[10];

int dev_format(char *name, int bsize, int bcount);

int dev_open(char *name);

int dev_read_block(int dh, char *buff, int bindex);

int dev_write_block(int dh, char *buff, int bindex);

int dev_close(int dh);

#endif
