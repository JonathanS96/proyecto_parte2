#include "device.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cont=0;
int menordisp=0;


int menorDisp(){
    for(cont=0; cont<10; cont++){
        if(files[cont]==NULL)
            return cont;
    }
    return 10;
}

int dev_format(char *name, int bsize, int bcount){
    FILE *f;
    char buffer[bsize];
    if(bsize>=44){
        memset(buffer, 0, bsize);
        f=fopen(name, "w");
        if(f!=NULL){
            if(bsize>=sizeof(struct Handle)){
                for(cont=0; cont<bcount+1; cont++){
                    fwrite(buffer, 1, bsize, f);
                }
                struct Handle *handle=(struct Handle*)buffer;
                memcpy(handle->signature, "JHSV", 4);
                memcpy(handle->name, name, 32);
                handle->blocksize=bsize;
                handle->blockcount=bcount;
                fseek(f, 0, SEEK_SET);
                fwrite(buffer, 1, bsize, f);
                fclose(f);
                return 0;
            }
        }
    }
    return -1;
}

int dev_open(char *name){
    menordisp=menorDisp();
    files[menordisp]=fopen(name, "r+");
    if(files[menordisp]!=NULL){
        fread(&deviceHandle[menordisp], 1, 44, files[menordisp]);
        //printf("hizo esto");
        if(memcmp(deviceHandle[menordisp].signature, "JHSV", 4)==0){
            //printf("%s, %s, %d, %d\n", deviceHandle[menordisp].signature, deviceHandle[menordisp].name, deviceHandle[menordisp].blocksize, deviceHandle[menordisp].blockcount);
            menordisp=menorDisp();
            return menordisp;
        }
    }
    return -1;
}


int dev_read_block(int dh, char *buffer, int bindex){
    memset(buffer, 0, sizeof(buffer));
    if(files[dh]!=NULL && bindex<=deviceHandle[dh].blockcount){
        fseek(files[dh], bindex*deviceHandle[dh].blocksize, SEEK_SET);
        //printf("%d\n",deviceHandle[dh].blocksize);
        fread(buffer, 1, deviceHandle[dh].blocksize, files[dh]);
        //printf("%s\n", buffer);
        return 0;
    }
    return -1;
}

int dev_write_block(int dh, char *buffer, int bindex){
    if(files[dh]!=NULL && bindex<=deviceHandle[dh].blockcount){
        fseek(files[dh], bindex*deviceHandle[dh].blocksize, SEEK_SET);
        fwrite(buffer, 1, deviceHandle[dh].blocksize, files[dh]);
        return 0;
    }
    return -1;
}

int dev_close(int dh){
    if(files[dh]!=NULL){
        fclose(files[dh]);
        files[dh]=NULL;
        return 0;
    }
    return -1;
}
