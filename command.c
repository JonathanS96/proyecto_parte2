#include <stdio.h>
#include <stdlib.h>		/* for free() */
#include <string.h>
#include <editline/readline.h>
#include "device.h"

char* param[5];
char buffer[1500];
int cont1=0;
int cont2=2;

void processLine(char *line)
{
    char *p;
    cont1=0;

    if (strcmp(line, "clear history") == 0) {
        clear_history();
    } else {
        add_history(line);
    }

    p = strtok(line, " ");
    //printf("Command is '%s'\n", p);
    param[cont1]=p;
    cont1++; cont2++;

    p = strtok(NULL, " ");
    while (p != NULL) {
        //printf("Argument: '%s'\n", p);
        param[cont1]=p;
        cont1++; cont2++;
        p = strtok(NULL, " ");
    }

}

int correr(int argc, char *argv[])
{
    const char *prompt = "$ ";
    char *line;

    for(cont1=0; cont1<10; cont1++){
        files[cont1]=(FILE*)1;
    }


    if (argc > 1) {
        ++argv, --argc; /* The first argument is the program name */
    }

    while (1) {
        line = readline(prompt);

        if (line == NULL) {
            break;
        }

        processLine(line);

        //CREATE DEVICE
        if(strcmp(param[0], "create")==0 && strcmp(param[1], "device")==0){
            if(param[2]!=NULL && param[3]!=NULL && param[4]!=NULL){
                dev_format(param[2], atoi(param[3]), atoi(param[4]));
            }
        }

        //OPEN DEVICE
        if(strcmp(param[0], "open")==0 && strcmp(param[1], "device")==0 && strcmp(param[3], "as")==0){
            if(param[2]!=NULL && param[4]!=NULL){
                if(atoi(param[4])>=0 && atoi(param[4])<10){
                    files[atoi(param[4])]=NULL;
                    dev_open(param[2]);
                }else{
                    printf("Ingrese un numero entre 0 y 9\n");
                }
            }
        }

        //SHOW OPEN DEVICES
        if(strcmp(param[0], "show")==0 && strcmp(param[1], "open")==0 && strcmp(param[2], "devices")==0){
            for(cont1=0; cont1<10; cont1++){
                if(files[cont1]!=(FILE*)1){
                    printf("%s, %d\n", deviceHandle[cont1].name, cont1);
                }
            }
        }

        //SHOW METADATA
        if(strcmp(param[0], "show")==0 && strcmp(param[1], "metadata")==0 && strcmp(param[2], "from")==0){
            if(param[3]!=NULL){
                if(atoi(param[3])>=0 && atoi(param[3])<10){
                    if(files[atoi(param[3])]!=(FILE*)1){
                        printf("%s, %d, %d\n", deviceHandle[atoi(param[3])].name, deviceHandle[atoi(param[3])].blocksize, deviceHandle[atoi(param[3])].blockcount);
                    }
                }else{
                    printf("Ingrese un numero entre 0 y 9\n");
                }
            }
        }

        //READ BLOCK
        if(strcmp(param[0], "read")==0 && strcmp(param[1], "block")==0){
            if(param[2]!=NULL && param[3]!=NULL){
                dev_read_block(atoi(param[2]), buffer, atoi(param[3]));

                if(cont2==4){
                    for(cont1=0; cont1<atoi(param[4]); cont1++){
                        printf("%x", buffer[cont1]);
                    }
                    printf("\n");
                }

                if(atoi(param[4])<sizeof(buffer)){
                    buffer[atoi(param[4])]=0;
                    for(cont1=0; cont1<atoi(param[4]); cont1++){
                        printf("%x", buffer[cont1]);
                    }
                    printf("\n");
                }

            }
        }

        //CLOSE DEVICE
        if(strcmp(param[0], "close")==0 && strcmp(param[1], "device")==0){
            if(param[2]!=NULL){
                files[atoi(param[2])]=(FILE*)1;
            }
        }

        if (*line == 0) {
            free(line);
	    continue;
        }

        if (strcmp(line, "exit") == 0 ||
                strcmp(line, "quit") == 0) {
            break;
        }

        if(strcmp(line, "prueba")==0){
            printf("Prueba");
        }
        cont2=0;
        free(line);
    }

    printf("\nExiting ...\n");

    return 0;
}
